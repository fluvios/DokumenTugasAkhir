\select@language {bahasa}
\contentsline {chapter}{Abstrak}{i}
\contentsline {chapter}{Abstract}{ii}
\contentsline {chapter}{Lembar Persembahan}{iii}
\contentsline {chapter}{Kata Pengantar}{vi}
\contentsline {chapter}{Daftar Isi}{vii}
\contentsline {chapter}{Daftar Gambar}{ix}
\contentsline {chapter}{Daftar Tabel}{xi}
\contentsline {chapter}{\numberline {I}Pendahuluan}{1}
\contentsline {section}{\numberline {1.1}Latar Belakang}{1}
\contentsline {section}{\numberline {1.2}Perumusan Masalah}{2}
\contentsline {section}{\numberline {1.3}Tujuan}{2}
\contentsline {section}{\numberline {1.4}Metode Penyelesaian Masalah}{3}
\contentsline {section}{\numberline {1.5}Sistematika Penulisan}{4}
\contentsline {chapter}{\numberline {II}Kajian Pustaka}{5}
\contentsline {section}{\numberline {2.1}E-learning Readiness}{5}
\contentsline {section}{\numberline {2.2}Multiple Criterion Decision Making}{7}
\contentsline {section}{\numberline {2.3}Analytic Hierarchy Process (AHP)}{8}
\contentsline {subsection}{\numberline {2.3.1}Consistency Test}{9}
\contentsline {section}{\numberline {2.4}Fuzzy Analytical Hierarchy Process (FAHP)}{10}
\contentsline {subsection}{\numberline {2.4.1}Triangular Fuzzy Numbers (TFNs)}{10}
\contentsline {subsection}{\numberline {2.4.2}Fuzzy Pair-Wise Comparison Matrix}{11}
\contentsline {subsection}{\numberline {2.4.3}Agregate the Group Decisions}{11}
\contentsline {subsection}{\numberline {2.4.4}Compute the Value of Fuzzy Synthetic Extent}{12}
\contentsline {subsection}{\numberline {2.4.5}Defuzzification}{12}
\contentsline {section}{\numberline {2.5}Black-box Testing}{12}
\contentsline {chapter}{\numberline {III}Metodologi dan Desain Sistem}{14}
\contentsline {section}{\numberline {3.1}Metodologi Penelitian}{14}
\contentsline {subsection}{\numberline {3.1.1}Identifikasi Masalah}{15}
\contentsline {subsection}{\numberline {3.1.2}Studi Literatur}{15}
\contentsline {subsection}{\numberline {3.1.3}Desain Sistem}{16}
\contentsline {subsubsection}{Requirements}{16}
\contentsline {subsubsection}{Analysis \& Design}{17}
\contentsline {subsubsection}{Alur Sistem}{17}
\contentsline {subsubsection}{Rekomendasi Model}{18}
\contentsline {subsubsection}{Perancangan Hirakri Keputusan}{19}
\contentsline {subsubsection}{Penilaian Kriteria, Sub Kriteria, dan Alternatif}{34}
\contentsline {subsubsection}{Uji Konsistensi}{35}
\contentsline {subsubsection}{Fuzzifikasi}{36}
\contentsline {subsubsection}{Komputasi Secara Fuzzy}{36}
\contentsline {subsubsection}{Defuzzifikasi}{37}
\contentsline {subsubsection}{Perhitungan Bobot Alternatif}{38}
\contentsline {subsubsection}{Pengurutan Model E-Learning Readiness}{38}
\contentsline {subsubsection}{Evaluasi E-Learning Readiness}{38}
\contentsline {subsubsection}{Use Case Diagram}{40}
\contentsline {subsubsection}{Class Diagram}{42}
\contentsline {subsubsection}{Activity Diagram}{44}
\contentsline {subsubsection}{Kebutuhan Perangkat Lunak}{44}
\contentsline {subsubsection}{Kebutuhan Perangkat Keras}{44}
\contentsline {subsection}{\numberline {3.1.4}Implementasi}{44}
\contentsline {subsection}{\numberline {3.1.5}Pengujian Sistem}{45}
\contentsline {subsection}{\numberline {3.1.6}Pengolahan Data Pengujian}{45}
\contentsline {subsection}{\numberline {3.1.7}Penarikan Kesimpulan}{45}
\contentsline {chapter}{\numberline {IV}Implementasi dan Pengujian}{46}
\contentsline {section}{\numberline {4.1}Implementasi Sistem}{46}
\contentsline {section}{\numberline {4.2}Skenario Pengujian}{47}
\contentsline {section}{\numberline {4.3}Hasil Implementasi}{47}
\contentsline {subsection}{\numberline {4.3.1}Hasil Rekomendasi Model E-Learning Readiness}{48}
\contentsline {subsection}{\numberline {4.3.2}Hasil Evaluasi E-Learning Readiness}{52}
\contentsline {subsubsection}{Evaluasi Pihak PJJ}{53}
\contentsline {chapter}{\numberline {V}Kesimpulan}{75}
\contentsline {section}{\numberline {5.1}Kesimpulan}{75}
\contentsline {section}{\numberline {5.2}Saran}{75}
\contentsline {chapter}{Daftar Pustaka}{76}
\contentsline {chapter}{Lampiran}{81}
